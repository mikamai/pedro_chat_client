package com.mikamai
{
	import com.adobe.serialization.json.JSON;
	
	import flash.events.EventDispatcher;
	
	[Bindable]
	public class Message extends EventDispatcher
	{		
		public var type:String;
		public var body:String;		
		public var author:String;
		public var status:String;
		public var typology:String;
		public var action:String;
		public var _date:Date;
		public var _position:String;
		public var spacer:Boolean;					
					
		public static var evenAuthorLabelColor:String 	= '999999';
		public static var oddAuthorLabelColor:String 	= '00989b';
							
		public function Message() 
		{			
			this.spacer = true;
			this._position = 'odd';
		}				
		
		public function isMessage():void
		{			
			this.type == 'message';
		}
		
		public function get authorLabelColor():String
		{
			return this._position == 'even' ? evenAuthorLabelColor : oddAuthorLabelColor;
		}
		
		public function set date(value:String):void
		{
			var time:Number = Date.parse(value);
			if (time)
			{
				this._date = new Date(time); 	
			}
		}
		
		public function get date():String
		{			
			if (this._date)
			{
				return this._date.hours.toString() + ':' + this._date.minutes.toString() + ' | ' + 
						 this._date.date.toString() + '.' + (this._date.month + 1).toString() + '.' + 
						 this._date.fullYear.toString();
			} else
			{
				return null;	
			}						
		}
		
		public function set position(value:String):void
		{
			
			this._position = value;	
		}
		
		public function get position():String
		{
			return this._position;
		}				
		
		public static function parse(jsonString:String):Message
		{			
			var message:Message = null;
			try {
				var obj:Object = JSON.decode(jsonString);
				message = new Message();
				message.type 		= obj.type;
				message.body 		= obj.body;
				message.author 		= obj.author;
				message.typology 	= obj.typology;
				message.action 		= obj.action;
				message.date 		= obj.date;																	
			} catch (e:Error) {
				trace('error parsing message: ' + e.message);
			} finally {
				return message;
			}
		}
		
		public function toHtml():String
		{
			var html:String = '<font face="Arial" size="12">' + this.htmlAuthor() + ' ' + this.htmlBody() + '<br>' + this.htmlDate() + '</font><br>';
			if (this.spacer) html = '<br>' + html;
			
			return html;
		}
		
		public function htmlAuthor():String
		{
			return '<font color="#' + this.authorLabelColor + '" size="+1"><b>' + this.author +  ':</b></font>';
		}
		
		public function htmlBody():String
		{
			var color:String = '222222';
			var text:String = this.body;
			if (this.typology)
			{
				switch (this.typology)
				{
					case 'system':
						text = '<i>' + text + '</i>';
						color = '585858';			
						break;
					case 'moderator':
						text = '<b>' + text + '</b>';
						color = 'cb1d2e';			
						break;
					case 'partner':
						text = '<b>' + text + '</b>';
						color = '0d517f';			
						break;
				}	
			}			
			
			return '<font color="#' + color + '">' + text +  '</font>';
		}
		
		public function htmlDate():String
		{
			var html:String = '';
			if (this.date)
			{
				html = '<font color="#676767" size="-3"><b>' + this.date +  '</b></font>';
			}
			
			return html;
		}

	}
}