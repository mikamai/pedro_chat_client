package com.mikamai
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.XMLSocket;
	import flash.utils.Timer;
	
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	
	public class MikaApplication extends mx.core.Application
	{
		import com.adobe.serialization.json.JSON;
		import mx.events.FlexEvent;
		import mx.controls.Alert;
		import mx.collections.ArrayCollection;
					
		public var socket:XMLSocket;
		public var messageList:List;
		public var inputText:TextInput;
		[Bindable]
		public var connected:Boolean;
		public var connectionTimer:Timer;
		[Bindable]
		public var messages:Array;
		[Bindable]
		public var statusMessage:String;
		
		private var chatHost:String;
		private var chatPort:Number;
		private var chatPolicyFilePort:Number;
			
		[Bindable]
		public var userName:String;
	
		public var conversation:TextArea;		
				
		public function MikaApplication()
		{					
			super();
			this.statusMessage = new String("");
			this.addEventListener(FlexEvent.APPLICATION_COMPLETE, this.init);												
		}
				
		private function handleFocusOnInpuText(event:Event):void
		{
			this.inputText.text = '';
			this.inputText.setStyle('color', '#222222');
			this.inputText.removeEventListener(FocusEvent.FOCUS_IN, this.handleFocusOnInpuText);
		}
				
		public function init(event:Event):void
		{
			this.inputText.addEventListener(FocusEvent.FOCUS_IN, this.handleFocusOnInpuText);		
			this.chatHost = Application.application.parameters.pedroChatHost;
			this.chatPort = Application.application.parameters.pedroChatPort;
			this.userName = Application.application.parameters.user;
			this.chatPolicyFilePort = 3000;
			this.messages = new Array();
			this.connected = false;
			this.connectionTimer = new Timer(4000);
			this.connectionTimer.addEventListener(TimerEvent.TIMER, this.connect);
			this.socket = new XMLSocket();
			
			this.socket.addEventListener(DataEvent.DATA, this.handleDataEvent);
			this.socket.addEventListener(Event.CONNECT, this.handleConnectEvent);
			this.socket.addEventListener(ProgressEvent.PROGRESS, this.handleProgressEvent);
			this.socket.addEventListener(Event.CLOSE, this.handleCloseEvent);
			this.socket.addEventListener(IOErrorEvent.IO_ERROR, this.handleIOErrorEvent);
			this.socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.handleSecurityErrorEvent);			
			inputText.addEventListener(FlexEvent.ENTER, this.sendMessage);
			this.connect();	
		}
		
		public function sendHeloMessage():void
		{
			var message:String = JSON.encode({author: this.userName, type: 'helo'});
			this.socket.send(message);
		}
		
		public function sendMessage(event:Event = null):void
		{
			if (inputText.text.length > 0 ) {
				var message:String = JSON.encode({author: this.userName, body: inputText.text});
				this.socket.send(message);
				inputText.text = "";	
			}			
		}			
		
		public function connect(event:TimerEvent = null):void
		{
			trace('connecting...');
			this.statusMessage = 'connecting...';
			this.socket.connect(this.chatHost, this.chatPort);
		}
		
		public function handleConnectEvent(event:Event):void
		{
			this.connected = true;
			this.statusMessage = 'connected';
			this.connectionTimer.stop();
			this.statusMessage = new String("");
			this.sendHeloMessage();
			debug('connected');
		}
		
		public function handleProgressEvent(event:Event):void
		{
			debug('progress');
		}					
		
		public function handleCloseEvent(event:Event):void
		{
			this.connected = false;
			this.startConnectionTimer();				
			debug('close');
			
		}
		
		public function handleDataEvent(event:DataEvent):void
		{
			trace('received data ' + event.data);
			this.statusMessage = "";
			var message:Message = Message.parse(event.data);
			if (message) 
			{
				if (message.type == 'message')
				{
					this.addMessage(message);
				}	
			}													 							
		}
		
		public function addMessage(message:Message):void
		{
			if (this.messages.length == 0) message.spacer = false;
			this.messages.push(message);			
			message.position = this.messages.length % 2 == 0 ? 'even' : 'odd';
			this.conversation.htmlText += message.toHtml();
			
			this.conversation.validateNow();			
			this.validateNow();
			
			this.conversation.verticalScrollPosition = this.conversation.maxVerticalScrollPosition + 10;																											
		}
		
		public function handleIOErrorEvent(event:IOErrorEvent):void
		{
			this.connected = false;
			this.startConnectionTimer();
			this.statusMessage =  'error connecting, retrying...';
		}
		
		public function handleSecurityErrorEvent(event:SecurityErrorEvent):void
		{				
			trace('Security error');
			this.statusMessage = 'Security error';
			this.connect();
		}						
		
		public function startConnectionTimer():void
		{
			this.connectionTimer.start();
		}
		
		public function debug(message:String):void
		{
			trace(message);
		}		

	}
}